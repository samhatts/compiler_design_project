typedef enum {integer,boolean,character,id,err,voi} DataTypes;
typedef enum{stati,fiel,argument,loca,functio,file} Kind;
typedef struct{
    char varname[128];
    DataTypes type;
    Kind kind;
    char function[128];
    int number;
} Symbol;
void initsymtab();
void insymbol(char* name,DataTypes type,Kind kind,char* function);
int FindSymbol(char* name);
int subcheck(char* name);
int functionsame(char* name,char* function);
int find_infunc(char* name,char* function);
