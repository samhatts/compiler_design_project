#include "symbols.h"
#include <string.h>
#include <stdio.h>
Symbol symtab[1000];
int numsym;

void initsymtab(){
    numsym=0;
}

void insymbol(char* name,DataTypes type,Kind kind,char* function){
    symtab[numsym].type=type;
    strcpy(symtab[numsym].varname,name);
    strcpy(symtab[numsym].function,function);
    symtab[numsym].kind=kind;
    int count;
    count=-1;
    int i;
    for(i=0;i<numsym;i++){
      if(symtab[i].kind==kind){
        count+=1;
      }
    }
    symtab[numsym].number=count;
    numsym++;
}

int FindSymbol(char* name){
  int i;
  if(numsym==0){
    return -1;
  }
  for(i=0;i<numsym;i++){
      if(!strcmp(symtab[i].varname,name)){
          return i;
      }
  }
  return -1;
}
int find_infunc(char* name,char* function){
  int i;
  if(numsym==0){
    return -1;
  }
  for(i=0;i<numsym;i++){
      if(!strcmp(symtab[i].varname,name)){
          if(!strcmp(symtab[i].function,function)){
            return i;
          }
      }
  }
  return -1;
}
int functionsame(char* name,char* function){
  int find;
  find=FindSymbol(name);
  if(find!=-1){
    if(!strcmp(symtab[find].function,function)){
      return 1;
    }
  }
  return -1;
}

int subcheck(char * name){
  int find;
  find=FindSymbol(name);
  if(find==-1){
    return -1;
  }
  if(symtab[find].kind==4||symtab[find].kind==5){
    return 1;
  }
  return -1;
  
}
