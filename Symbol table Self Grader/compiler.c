/************************************************************************
University of Leeds
School of Computing
COMP2932- Compiler Design and Construction
The Compiler Module

I confirm that the following code has been developed and written by me and it is entirely the result of my own work.
I also confirm that I have not copied any parts of this program from another person or any other source or facilitated someone to copy this program from me.
I confirm that I will not publish the program online or share it with anyone without permission of the module leader.

Student Name:
Student ID:
Email:
Date Work Commenced:
*************************************************************************/

#include "compiler.h"
#include "parser.h"
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
int passnumber;

int InitCompiler ()
{
	ParserInfo p;
	initsymtab();
	return 1;
}

ParserInfo compile (char* dir_name)
{
	ParserInfo p;
	passnumber=0;
	p.er = 0;
	// write your code below
	insymbol("Array",3,5,"program");
	p=Parse("Array.jack");
	insymbol("Keyboard",3,5,"program");
	p=Parse("Keyboard.jack");
	insymbol("Math",3,5,"program");
	p=Parse("Math.jack");
	insymbol("Memory",3,5,"program");
	p=Parse("Memory.jack");
	insymbol("Output",3,5,"program");
	p=Parse("Output.jack");
	insymbol("Screen",3,5,"program");
	p=Parse("Screen.jack");
	insymbol("Sys",3,5,"program");
	p=Parse("Sys.jack");
	insymbol("String",3,5,"program");
	p=Parse("String.jack");
	char* dir;
    char* basedir;
	char* newdir;
	char s[100];
	char t[100];
	char u[100];
	dir=getcwd(s,100);
	basedir=getcwd(t,100);
	DIR *dp;
	struct dirent* entry;
	strcat(dir,"/");
	char* third = (char*)malloc(strlen(dir)+strlen(dir_name)+2);
	third[0]='\0';
	strcat(third,dir);
	strcat(third,dir_name);
	chdir(third);
	dp=opendir(third);
	while((entry=readdir(dp))){
		char* name;
		char othername[128];
		name=entry->d_name;
		if(*name!='.'){
			int i;
			int length;
			length=strlen(name);
			for(i=0;i<length;i++){
				if(name[i]!='.'){
					othername[i]=name[i];
				}
				else{
					break;
				}
			}
			othername[i]='\0';
			insymbol(othername,3,5,"program");
		}
		if(entry==0){
			break;
		}
	}
	DIR *dp2;
	struct dirent* entry2;
	chdir(third);
	dp2=opendir(third);
	while((entry2=readdir(dp2))){
		char* name2;
		name2=entry2->d_name;
		if(*name2!='.'){
			p=Parse(name2);
			if(p.er!=0){
				chdir(basedir);
				return p;
			}
		}
		if(entry2==0){
			chdir(basedir);
			break;
		}
	}
	chdir(basedir);
	passnumber=1;
	DIR *dp3;
	struct dirent* entry3;
	chdir(third);
	dp3=opendir(third);
	while((entry3=readdir(dp3))){
		char* name3;
		name3=entry3->d_name;
		if(*name3!='.'){
			p=Parse(name3);
			if(p.er!=0){
				chdir(basedir);
				return p;
			}
		}
		if(entry3==0){
			chdir(basedir);
			break;
		}
	}
	chdir(basedir);
	return p;
}

int StopCompiler ()
{


	return 1;
}

#ifndef TEST_COMPILER
/*int main ()
{
	printf("-1");
	InitCompiler ();
	ParserInfo p = compile ("Pong");
	PrintError (p);
	StopCompiler ();
	return 1;
}*/
#endif
