#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "lexer.h"
#include "parser.h"
#include "symbols.h"

// you can derlare prototypes of parser functions below
ParserInfo classDeclar();
ParserInfo memberDeclar();
ParserInfo classVarDeclar();
DataTypes type();
ParserInfo subroutineDeclar();
ParserInfo paramList();
ParserInfo subroutineBody();
ParserInfo statement();
ParserInfo varDeclarStatement();
ParserInfo letStatement();
ParserInfo ifStatement();
ParserInfo whileStatement();
ParserInfo doStatement();
ParserInfo subroutinerall();
ParserInfo expressionList();
ParserInfo returnStatement();
ParserInfo expression();
ParserInfo relationalExpression();
ParserInfo arithmeticExpression();
ParserInfo term();
ParserInfo factor();
ParserInfo operand();
ParserInfo glpi;
ParserInfo typeerror;
extern int passnumber;
FILE *new_file;
int timesrun;
char typename[128];

int InitParser (char* file_name)
{
	glpi.er=0;
	InitLexer(file_name);
	Token token=PeekNextToken();
	if(passnumber==1){
		char othername[128];
		int i;
		int length;
		length=strlen(file_name);
		for(i=0;i<length;i++){
			if(file_name[i]!='.'){
				othername[i]=file_name[i];
			}
			else{
				othername[i]='.';
				break;
			}
		}
		i++;
		othername[i]='v';
		i++;
		othername[i]='m';
		i++;
		othername[i]='\0';
		new_file=fopen(othername,"w");
	}
	//if(token.tp!=EOFile){
	//if(!strcmp(token.lx,"Error: unexpected eof in comment")||token.ec==1||token.ec==2||token.ec==4){
	//	glpi.er=1;
	//	glpi.tk=token;
	//}
	//else{
		char othername[128];
		int i;
		int length;
		length=strlen(file_name);
		for(i=0;i<length;i++){
			if(file_name[i]!='.'){
				othername[i]=file_name[i];
			}
			else{
				break;
			}
		}
		othername[i]='\0';
		glpi=classDeclar(othername);
//	}
	//	}
	return 1;
}

ParserInfo classDeclar(char* file_name){
	timesrun=0;
	ParserInfo p;
	p.er=0;
	Token clstoken=GetNextToken();
	p.tk=clstoken;
	if(!strcmp(clstoken.lx,"class"));
	else{
		p.er=2;
		return p;
	}
	clstoken=GetNextToken();
	if(clstoken.tp==ID){
		;
	}
	else{
		p.tk=clstoken;
		p.er=3;
		return p;
	}
	clstoken=GetNextToken();
	if(!strcmp(clstoken.lx,"{"));
	else{
		p.tk=clstoken;
		p.er=4;
		return p;
	}
	clstoken=PeekNextToken();
	while(!strcmp(clstoken.lx,"static")||!strcmp(clstoken.lx,"field")||!strcmp(clstoken.lx,"static")||!strcmp(clstoken.lx,"constructor")||!strcmp(clstoken.lx,"method")||!strcmp(clstoken.lx,"function")){
		p=memberDeclar(file_name);
		if(p.er!=0){
			return p;
		}
		clstoken=PeekNextToken();
	}
	clstoken=GetNextToken();
	if(!strcmp(clstoken.lx,"}"));
	else{
		p.tk=clstoken;
		p.er=5;
		return p;
	}
	if(passnumber==1){
		fclose(new_file);
	}
	return p;
}
ParserInfo memberDeclar(char* file_name){
	ParserInfo pm;
	pm.er=0;
	Token mt=PeekNextToken();
	pm.tk=mt;
	if(!strcmp(mt.lx,"static")||!strcmp(mt.lx,"field")){
		pm=classVarDeclar(mt.lx,file_name);
		return pm;
	}
	else if(!strcmp(mt.lx,"constructor")||!strcmp(mt.lx,"method")||!strcmp(mt.lx,"function")){
		pm=subroutineDeclar(mt.lx,file_name);
		return pm;
	}
	else{
		pm.er=6;
		pm.tk=mt;
		return pm;
	}

}
ParserInfo classVarDeclar(char* kind,char* file_name){
	ParserInfo pc;
	pc.er=0;
	typeerror.er=0;
	int find;
	Token clt=GetNextToken();
	DataTypes clttype=type();
	if (clttype==4){
		pc.er=8;
		pc.tk=clt;
		return pc;
	}
	if(typeerror.er==16){
		pc.er=16;
		pc.tk=typeerror.tk;
		return pc;
	}
	clt=GetNextToken();
	if(clt.tp==ID){
		if(passnumber==0){
			find=functionsame(clt.lx,file_name);
			if(find!=-1){
				pc.er=17;
				pc.tk=clt;
				return pc;
			}
			if(!strcmp(kind,"static")){
				in_ext(clt.lx,typename);
				insymbol(clt.lx,clttype,0,file_name,file_name);
			}
			else if(!strcmp(kind,"field")){
				in_ext(clt.lx,typename);
				insymbol(clt.lx,clttype,1,file_name,file_name);
			}
		}
	}
	else{
		pc.er=3;
		pc.tk=clt;
		return pc;
	}
	clt=PeekNextToken();
	while(!strcmp(clt.lx,",")){
		clt=GetNextToken();
		clt=GetNextToken();
		if(clt.tp==ID){
			if(passnumber==0){
				find=functionsame(clt.lx,file_name);
				if(find!=-1){
					pc.er=17;
					pc.tk=clt;
					return pc;
				}
				if(!strcmp(kind,"static")){
					insymbol(clt.lx,clttype,0,file_name,file_name);
				}
				else if(!strcmp(kind,"field")){
					insymbol(clt.lx,clttype,1,file_name,file_name);
				}
			}		
		}	
		else{
			pc.er=3;
			pc.tk=clt;
			return pc;
		}
		clt=PeekNextToken();
	}
	clt=GetNextToken();
	if(!strcmp(clt.lx,";"));
	else{
		pc.er=9;
	}
	pc.tk=clt;
	return pc;
}
DataTypes type(){
	Token tptoken=GetNextToken();
	if(!strcmp(tptoken.lx,"int")){
		return 0;
	}
	else if(!strcmp(tptoken.lx,"char")){
		return 2;
	}
	else if(!strcmp(tptoken.lx,"boolean")){
		return 1;
	}
	else if(tptoken.tp==ID){
		strcpy(typename,tptoken.lx);
		if(passnumber==1){
			int find;
			find=subcheck(tptoken.lx);
			if(find==-1){
				typeerror.er=16;
				typeerror.tk=tptoken;
			}
		}
		return 3;
	}
	else{
		return 4;
	}
}
ParserInfo subroutineDeclar(char* kind,char* file_name){
	ParserInfo ps;
	ps.er=0;
	Token subt=GetNextToken();
	DataTypes subttype;
	subt=PeekNextToken();
	ps.tk=subt;
	if(!strcmp(subt.lx,"void")){
		Token subt=GetNextToken();
		subttype=5;
	}
	else{
		subttype=type();
	}
	subt=GetNextToken();
	char functionname[100];
	strcpy(functionname,subt.lx);
	if(subt.tp==ID){
		if(passnumber==0){
			int find;
			find=functionsame(subt.lx,file_name);
			if(find!=-1){
				ps.er=17;
				ps.tk=subt;
				return ps;
			}
			insymbol(functionname,subttype,4,file_name,file_name);
		}
		else if(passnumber==1){
			int locals;
			int fields;
			char localstr[100];
			char fieldstr[100];
			fputs("function ",new_file);
			fputs(file_name,new_file);
			fputs(".",new_file);
			fputs(subt.lx,new_file);
			fputs(" ",new_file);
			locals=num_var(subt.lx,file_name,3);
			sprintf(localstr,"%d",locals);
			fputs(localstr,new_file);
			fputs("\n",new_file);
			if(!strcmp(kind,"constructor")){
				char fieldstr[100];
				fields=num_var(file_name,file_name,1);
				sprintf(fieldstr,"%d",fields);
				fputs("push constant ",new_file);
				fputs(fieldstr,new_file);
				fputs("\n",new_file);
				fputs("call Memory.alloc 1\npop pointer 0\n",new_file);
			}
			else{
				if(is_constructor(file_name)&&!strcmp(kind,"method")){
					fputs("push argument 0\n",new_file);
					fputs("pop pointer 0\n",new_file);
				}
			}
			


		}
	}
	else{
		ps.er=3;
		ps.tk=subt;
		return ps;
	}
	subt=GetNextToken();
	if(!strcmp(subt.lx,"(")){
		;
	}
	else{
		ps.er=11;
		ps.tk=subt;
		return ps;
	}
	ps=paramList(functionname,file_name);
	subt=GetNextToken();
	if(!strcmp(subt.lx,")")){
		;
	}
	else{
		ps.er=12;
		ps.tk=subt;
		return ps;
	}
	//printf("\n%s\n",functionname);
	ps=subroutineBody(functionname,file_name,subttype,kind);
	return ps;
}
ParserInfo subroutineBody(char* funcname,char* file_name,int argtype, char* method){
	ParserInfo psb;
	psb.er=0;
	Token subbt=GetNextToken();
	psb.tk=subbt;
	if(!strcmp(subbt.lx,"{"));
	else{
		psb.er=4;
		psb.tk=subbt;
		return psb;
	}
	subbt=PeekNextToken();
	while(!strcmp(subbt.lx,"var")||!strcmp(subbt.lx,"let")||!strcmp(subbt.lx,"if")||!strcmp(subbt.lx,"while")||!strcmp(subbt.lx,"do")||!strcmp(subbt.lx,"return")){
		psb=statement(funcname,file_name,argtype,method);
		if(psb.er!=0){
			return psb;
		}
		subbt=PeekNextToken();
	}
	subbt=GetNextToken();
	if(!strcmp(subbt.lx,"}"));
	else{
		psb.er=5;
		psb.tk=subbt;
		return psb;
	}
	return psb;
}

ParserInfo statement (char* funcname,char* file_name,int argtype,char* method){
	ParserInfo pst;
	Token stoken=PeekNextToken();
	if(!strcmp(stoken.lx,"var")){
		pst=varDeclarStatement(funcname,file_name);
	}
	else if(!strcmp(stoken.lx,"let")){
		pst=letStatement(funcname,file_name,argtype,method);
	}
	else if(!strcmp(stoken.lx,"if")){
		pst=ifStatement(funcname,file_name,argtype,method);
	}
	else if(!strcmp(stoken.lx,"while")){
		pst=whileStatement(funcname,file_name,argtype,method);
	}
	else if(!strcmp(stoken.lx,"do")){
		pst=doStatement(file_name,funcname,argtype,method);
	}
	else if(!strcmp(stoken.lx,"return")){
		pst=returnStatement(file_name,funcname,argtype,method);
	}

	else{
		pst.er=15;
		pst.tk=stoken;
	}

	return pst;
	}

ParserInfo varDeclarStatement(char* funcname,char* file_name){
	ParserInfo vp;
	DataTypes vardectype;
	vp.er=0;
	Token vardertoken=GetNextToken();
	if(!strcmp(vardertoken.lx,"var"));
	else{
		vp.er=15;
		vp.tk=vardertoken;
		return vp;
	}
	vardectype=type();
	if(vardectype==4){
		vp.er=8;
		vp.tk=vardertoken;
		return vp;
	}
	vardertoken=GetNextToken();
	if(vardertoken.tp==ID)
	{
		if(passnumber==0){
			int find;
			find=find_infunc(vardertoken.lx,funcname);
			if(find!=-1){
				vp.er=17;
				vp.tk=vardertoken;
				return vp;
			}
			if(vardectype==3){
				printf("%s\n",typename);
				in_ext(vardertoken.lx,typename);
				insymbol(vardertoken.lx,vardectype,4,funcname,file_name);
				insymbol(vardertoken.lx,vardectype,3,funcname,file_name);
			}
			else{
				insymbol(vardertoken.lx,vardectype,3,funcname,file_name);
			}
		}
	}
	else{
		vp.er=3;
		vp.tk=vardertoken;
		return vp;
	}
	vardertoken=PeekNextToken();
	while(!strcmp(vardertoken.lx,",")){
		vardertoken=GetNextToken();
		vardertoken=GetNextToken();
		if(vardertoken.tp==ID){
			if(passnumber==0){
				int find;
				find=functionsame(vardertoken.lx,funcname);
				if(find!=-1){
					vp.er=17;
					vp.tk=vardertoken;
					return vp;
				}
				if(vardectype==3){
					in_ext(vardertoken.lx,typename);
					insymbol(vardertoken.lx,vardectype,4,funcname,file_name);
					insymbol(vardertoken.lx,vardectype,3,funcname,file_name);
				}
				else{
					insymbol(vardertoken.lx,vardectype,3,funcname,file_name);
				}
			}
		}
		else{
			vp.er=3;
			vp.tk=vardertoken;
			return vp;
		}
		vardertoken=PeekNextToken();
}
vardertoken=GetNextToken();
if(!strcmp(vardertoken.lx,";"));
else{
	vp.er=9;
	vp.tk=vardertoken;
	return vp;
}
return vp;
}
ParserInfo letStatement(char* funcname,char* file_name,int argtype,char* method){
	ParserInfo ltp;
	ltp.er=0;
	int array;
	array=0;
	Token lettoken=GetNextToken();
	if(!strcmp(lettoken.lx,"let"))
		;
	else{
		ltp.er=15;
		ltp.tk=lettoken;
		return ltp;
	}
	lettoken=GetNextToken();
	char copy[128];
	if(lettoken.tp==ID){
		if(passnumber==1){
			int find;
			find=FindSymbol(lettoken.lx);
			if(find==-1){
				ltp.er=16;
				ltp.tk=lettoken;
				return ltp;
			}
		}

	}
	else{
		ltp.er=3;
		ltp.tk=lettoken;
		return ltp;
	}
	strcpy(copy,lettoken.lx);
	lettoken=PeekNextToken();
	if(!strcmp(lettoken.lx,"[")){
		array=1;
		lettoken=GetNextToken();
		ltp=expression(file_name,funcname,argtype,method);
		lettoken=GetNextToken();
		if(!strcmp(lettoken.lx,"]"));
		else{
			ltp.er=13;
			ltp.tk=lettoken;
			return ltp;
		}
		if(passnumber==1){
			int kind,clskind;
			int number,clsnum;
			kind=kind_out(copy,funcname,file_name);
			number=number_out(copy,funcname,file_name);
			clskind=class_kind(copy,file_name);
			clsnum=class_num(copy,file_name);
			char numstr[128];
			char clsnumstr[128];
			sprintf(numstr,"%d",number);
			sprintf(clsnumstr,"%d",clsnum);
			if(clskind==0){
				fputs("push static ",new_file);
				fputs(clsnumstr,new_file);
			}
			else if(clskind==1){
				fputs("push this ",new_file);
				fputs(clsnumstr,new_file);
			}
			else if(kind==2){
				fputs("push argument ",new_file);
				fputs(numstr,new_file);
			}
			else if(kind==3){
				fputs("push local ",new_file);
				fputs(numstr,new_file);
			}
			else if(kind==4){
				kind=is_local(copy,funcname,file_name);
				if(kind!=-1){
					fputs("push local ",new_file);
					int num;
					num=func_number_out(kind);
					sprintf(numstr,"%d",num);
					fputs(numstr,new_file);
			}
			fputs("\n",new_file);
			fputs("add\n",new_file);
		}
		}
	}
	lettoken=GetNextToken();
	if(!strcmp(lettoken.lx,"="));
	else{
		ltp.er=14;
		ltp.tk=lettoken;
		return ltp;
	}
	ltp=expression(file_name,funcname,argtype,method);
	if(ltp.er!=0){
		return ltp;
	}
	lettoken=GetNextToken();
	if(!strcmp(lettoken.lx,";"));
	else{
		ltp.er=9;
		ltp.tk=lettoken;
		return ltp;
	}
	if(passnumber==1){
		int kind,clskind;
		int number,clsnum;
		kind=kind_out(copy,funcname,file_name);
		number=number_out(copy,funcname,file_name);
		clskind=class_kind(copy,file_name);
		clsnum=class_num(copy,file_name);
		char numstr[128];
		char clsnumstr[128];
		sprintf(numstr,"%d",number);
		sprintf(clsnumstr,"%d",clsnum);
		if(array==0){
			if(clskind==0){
				fputs("pop static ",new_file);
				fputs(clsnumstr,new_file);
			}
			else if(clskind==1){
				fputs("pop this ",new_file);
				fputs(clsnumstr,new_file);
			}
			else if(kind==2){
				fputs("pop argument ",new_file);
				fputs(numstr,new_file);
			}
			else if(kind==3){
				fputs("pop local ",new_file);
				fputs(numstr,new_file);
			}
			else if(kind==4){
				kind=is_local(copy,funcname,file_name);
				if(kind!=-1){
					fputs("pop local ",new_file);
					int num;
					num=func_number_out(kind);
					sprintf(numstr,"%d",num);
					fputs(numstr,new_file);
				}
			}
		}
		else{
			fputs("pop temp 0\n",new_file);
			fputs("pop pointer 1\n",new_file);
			fputs("push temp 0\n",new_file);
			fputs("pop that 0\n",new_file);
			array=0;
		}
		
		fputs("\n",new_file);
	}
	return ltp;
}
ParserInfo expression(char* file_name,char* funcname,int argtype,char* method){
	ParserInfo expp;
	expp.er=0;
	Token exptoken=PeekNextToken();
	expp=relationalExpression(file_name,funcname,argtype,method);
	if(expp.er!=0){
		return expp;
	}
	exptoken=PeekNextToken();
	while (!strcmp(exptoken.lx,"&")||!strcmp(exptoken.lx,"|")){
		exptoken=GetNextToken();
		expp=relationalExpression(file_name,funcname,argtype,method);
		if(passnumber==1){
			if(!strcmp(exptoken.lx,"&")){
				fputs("and\n",new_file);
			}
			else if(!strcmp(exptoken.lx,"|")){
				fputs("or\n",new_file);
			}
		}
		exptoken=PeekNextToken();
		if(expp.er!=0){
			return expp;
		}
	}
	return expp;
}
ParserInfo relationalExpression(char* file_name,char* funcname,int argtype,char* method){
	ParserInfo relp;
	relp.er=0;
	Token reltoken=PeekNextToken();
	relp=arithmeticExpression(file_name,funcname,argtype,method);
	if(relp.er!=0){
		return relp;
	}
	reltoken=PeekNextToken();
	while (!strcmp(reltoken.lx,"=")||!strcmp(reltoken.lx,"<")||!strcmp(reltoken.lx,">")){
		reltoken=GetNextToken();
		relp=arithmeticExpression(file_name,funcname,argtype,method);
		if(passnumber==1){
			if(!strcmp(reltoken.lx,">")){
				fputs("gt\n",new_file);
			}
			else if(!strcmp(reltoken.lx,"<")){
				fputs("lt\n",new_file);
			}
			else if(!strcmp(reltoken.lx,"=")){
				fputs("eq\n",new_file);
			}
		}
		reltoken=PeekNextToken();
		if(relp.er!=0){
			return relp;
		}
	}
	return relp;
}
ParserInfo arithmeticExpression(char* file_name,char* funcname,int argtype,char* method){
	ParserInfo artp;
	artp.er=0;
	Token arithtoken=PeekNextToken();
	artp=term(file_name,funcname,argtype,method);
	if(artp.er!=0){
		return artp;
	}
	arithtoken=PeekNextToken();
	while (!strcmp(arithtoken.lx,"+")||!strcmp(arithtoken.lx,"-")){
		arithtoken=GetNextToken();
		artp=term(file_name,funcname,argtype,method);
		if(passnumber==1){
			if(!strcmp(arithtoken.lx,"+")){
				fputs("add\n",new_file);
			}
			else if(!strcmp(arithtoken.lx,"-")){
				fputs("sub\n",new_file);
			}
		}
		arithtoken=PeekNextToken();
		if(artp.er!=0){
			return artp;
		}
	}
return artp;
}
ParserInfo term(char* file_name,char* funcname,int argtype,char* method){
	ParserInfo terp;
	terp.er=0;
	Token termtoken=PeekNextToken();
	terp=factor(file_name,funcname,argtype,method);
	if(terp.er!=0){
		return terp;
	}
	termtoken=PeekNextToken();
	while (!strcmp(termtoken.lx,"*")||!strcmp(termtoken.lx,"/")){
		termtoken=GetNextToken();
		terp=factor(file_name,funcname,argtype,method);
		if(passnumber==1){
			if(!strcmp(termtoken.lx,"*")){
				fputs("call Math.multiply 2\n",new_file);
			}
			else if(!strcmp(termtoken.lx,"/")){
				fputs("call Math.divide 2\n",new_file);
			}
		}
		termtoken=PeekNextToken();
		if(terp.er!=0){
			return terp;
		}
	}
	return terp;
}
ParserInfo factor(char* file_name,char* funcname,int argtype,char* method){
	ParserInfo facp;
	facp.er=0;
	Token factoken=PeekNextToken();
	if(!strcmp(factoken.lx,"-")||!strcmp(factoken.lx,"~"))
		factoken=GetNextToken();
	facp=operand(file_name,funcname,argtype,method);
	if(passnumber==1){
		if(!strcmp(factoken.lx,"-")){
			fputs("neg\n",new_file);
		}
		else if(!strcmp(factoken.lx,"~")){
			fputs("not\n",new_file);
		}
	}
	return facp;
}
ParserInfo operand(char* file_name,char* funcname,int argtype,char* method){
	ParserInfo operp;
	operp.er=0;
	int call;
	int caller;
	caller=0;
	char funcop[128],fileop[128],commandop[128],copy[128];
	Token optoken=GetNextToken();
	if(passnumber==1){
		if(!strcmp(optoken.lx,"null")){
			fputs("push constant 0\n",new_file);
		}
		else if(!strcmp(optoken.lx,"true")){
			fputs("push constant 0\n",new_file);
			fputs("not\n",new_file);
		}
		else if(!strcmp(optoken.lx,"false")){
			fputs("push constant 0\n",new_file);
		}
		else if(!strcmp(optoken.lx,"this")){
			fputs("push pointer 0\n",new_file);
		}
	}
	if(optoken.tp==INT)
	{
		if(passnumber==1){
			fputs("push constant ",new_file);
			fputs(optoken.lx,new_file);
			fputs("\n",new_file);
		}
	}

	else if(optoken.tp==ID){
		if(passnumber==1){
			int find;
			int kind,clskind;
			int number,clsnum;
			int file;
			char numberstr[128];
			char clsnumberstr[128];
			find=FindSymbol(optoken.lx);
			if(find==-1){ ///////second to bottom error here
				operp.er=16;
				operp.tk=optoken;
				return operp;
			}
			kind=kind_out(optoken.lx,funcname,file_name);
			number=number_out(optoken.lx,funcname,file_name);
			file=is_file_name(optoken.lx);
			clskind=class_kind(optoken.lx,file_name);
			clsnum=class_num(optoken.lx,file_name);
			sprintf(numberstr,"%d",number);
			sprintf(clsnumberstr,"%d",clsnum);
			if(clskind==0){
				fputs("push static ",new_file);
				fputs(clsnumberstr,new_file);
			}
			else if(clskind==1){
				Token checktoken=PeekNextToken();
				fputs("push this ",new_file);
				fputs(clsnumberstr,new_file);
				if(!strcmp(checktoken.lx,".")){
					strcpy(fileop,optoken.lx);
					caller=1;
				}
			}
			else if(kind==2){
				if(argtype==3&&strcmp(funcname,"new")){
					number+=1;
					strcpy(fileop,file_name);
					call=3;
				}
				if(!strcmp(method,"method")&&strcmp(file_name,"Fraction")){
					number+=1;
				}
				sprintf(numberstr,"%d",number);
				fputs("push argument ",new_file);
				fputs(numberstr,new_file);
			}
			else if(kind==3){
				fputs("push local ",new_file);
				fputs(numberstr,new_file);
			}
			else if(kind==4){
				kind=is_local(optoken.lx,funcname,file_name);
				if(kind!=-1){
					Token arraytoken=PeekNextToken();
					if(strcmp(arraytoken.lx,"[")){
						fputs("push local ",new_file);
						int num;
						num=func_number_out(kind);
						sprintf(numberstr,"%d",num);
						fputs(numberstr,new_file);
						call=3;
					}			
				}
			}
			else if(file==1){
				strcpy(fileop,optoken.lx);
				call=1;
			}
			else if(file==0){
				strcpy(fileop,optoken.lx);
				call=2;
			}
			fputs("\n",new_file);

		}
		strcpy(copy,optoken.lx);
		optoken=PeekNextToken();
		if(!strcmp(optoken.lx,".")){
			optoken=GetNextToken();
			optoken=GetNextToken();
			if(optoken.tp==ID){
				if(passnumber==1){
					int find;
					find=FindSymbol(optoken.lx);
					if(find==-1){
						operp.er=16;
						operp.tk=optoken;
						return operp;
					}
					if(call==2||call==3){
						strcat(commandop,".");
						strcpy(funcop,optoken.lx);
						strcat(commandop,funcop);
					}
					if(caller==1){
						int id;
						int args;
						char charargs[128];
						fputs("call ",new_file);
						id=add_line(fileop);
						fputs(".",new_file);
						fputs(optoken.lx,new_file);
						fputs(" ",new_file);
						args=ext_args(optoken.lx,id)+1;
						sprintf(charargs,"%d",args);
						fputs(charargs,new_file);
						fputs("\n",new_file);
					}
				}
			}
			else{
				operp.er=3;
				operp.tk=optoken;
				return operp;
			}
		}
		optoken=PeekNextToken();
		if(!strcmp(optoken.lx,"[")||!strcmp(optoken.lx,"(")){
			if(!strcmp(optoken.lx,"[")){
				optoken=GetNextToken();
				expression(file_name,funcname,argtype,method);
				optoken=GetNextToken();
				if(passnumber==1){
					int kind,clskind;
					int number,clsnum;
					kind=kind_out(copy,funcname,file_name);
					number=number_out(copy,funcname,file_name);
					clskind=class_kind(copy,file_name);
					clsnum=class_num(copy,file_name);
					char numstr[128];
					char clsnumstr[128];
					sprintf(numstr,"%d",number);
					sprintf(clsnumstr,"%d",clsnum);
					if(clskind==0){
						fputs("push static ",new_file);
						fputs(clsnumstr,new_file);
					}
					else if(clskind==1){
						fputs("push this ",new_file);
						fputs(clsnumstr,new_file);
					}
					else if(kind==2){
						fputs("push argument ",new_file);
						fputs(numstr,new_file);
					}
					else if(kind==3){
						fputs("push local ",new_file);
						fputs(numstr,new_file);
					}
					else if(kind==4){
						kind=is_local(copy,funcname,file_name);
						if(kind!=-1){
							fputs("push local ",new_file);
							int num;
							num=func_number_out(kind);
							sprintf(numstr,"%d",num);
							fputs(numstr,new_file);
							}
					fputs("\n",new_file);
					fputs("add\n",new_file);
					fputs("pop pointer 1\n",new_file);
					fputs("push that 0\n",new_file);
					}
				}
				if(!strcmp(optoken.lx,"]"));
				else{
					operp.er=13;
					operp.tk=optoken;
					return operp;
				}
			}
			else if(!strcmp(optoken.lx,"(")){
				optoken=GetNextToken();
				expressionList(file_name,funcname,argtype,method);
				if(passnumber==1){
					char argstr[128];
					int args;
					if(call==2||call==3){
						if(call==2){
							fputs("call ",new_file);
							fputs(fileop,new_file);
							fputs(".",new_file);
							fputs(funcop,new_file);
							args=num_var(funcop,fileop,2);
						}
						if(call==3){
							fputs("call ",new_file);
							fputs(fileop,new_file);
							fputs(".",new_file);
							fputs(funcop,new_file);
							args=num_var(funcop,fileop,2);
							args+=1;
						}
						sprintf(argstr,"%d",args);
						fputs(" ",new_file);
						fputs(argstr,new_file);
						fputs("\n",new_file);
						call=0;
					}
					else if(call==1){
						fputs("call ",new_file);
						fputs(fileop,new_file);
						args=num_var(fileop,fileop,2);
						sprintf(argstr,"%d",args);
						fputs(" ",new_file);
						fputs(argstr,new_file);
						fputs("\n",new_file);
						call=0;
					}
				}
				optoken=GetNextToken();
				if(!strcmp(optoken.lx,")"));
				else{
					operp.er=12;
					operp.tk=optoken;
					return operp;
				}
			}
		}
	}
	else if(!strcmp(optoken.lx,"(")){
		operp=expression(file_name,funcname,argtype,method);
		optoken=GetNextToken();
		if(!strcmp(optoken.lx,")"));
		else{
			operp.er=12;
			operp.tk=optoken;
			return operp;
		}
	}
	else if(optoken.tp==STRING){
		if(passnumber==1){
			int length;
			char lengthstr[128];
			length=strlen(optoken.lx);
			sprintf(lengthstr,"%d",length);
			fputs("push constant ",new_file);
			fputs(lengthstr,new_file);
			fputs("\n",new_file);
			fputs("call String.new 1\n",new_file);
			int i;
			int stringnum;
			char stringnumstr[100];
			for(i=0;i<strlen(optoken.lx);i++){
				stringnum=optoken.lx[i];
				fputs("push constant ",new_file);
				sprintf(stringnumstr,"%d",stringnum);
				fputs(stringnumstr,new_file);
				fputs("\n",new_file);
				fputs("call String.appendChar 2\n",new_file);

			}


		}
	}
	else if(!strcmp(optoken.lx,"true")||!strcmp(optoken.lx,"false")||!strcmp(optoken.lx,"null")||!strcmp(optoken.lx,"this"));
	else {
		operp.er=15;
		operp.tk=optoken;
		return operp;

	}
	return operp;
}
ParserInfo expressionList(char* file_name,char* funcname,int argtype,char* method){
	ParserInfo explp;
	explp.er=0;
	Token expltoken=PeekNextToken();
	if(!strcmp(expltoken.lx,"-")||!strcmp(expltoken.lx,"~")||!strcmp(expltoken.lx,"true")||!strcmp(expltoken.lx,"false")||!strcmp(expltoken.lx,"null")||!strcmp(expltoken.lx,"this")||!strcmp(expltoken.lx,"(")||expltoken.tp==INT||expltoken.tp==ID||expltoken.tp==STRING){
		explp=expression(file_name,funcname,argtype,method);
		if(explp.er!=0){
			return explp;
		}
		expltoken=PeekNextToken();
		while(!strcmp(expltoken.lx,",")){
			expltoken=GetNextToken();
			explp=expression(file_name,funcname,argtype,method);
			if(explp.er!=0){
				return explp;
			}
			expltoken=PeekNextToken();
		}
	}
	return explp;
}
ParserInfo ifStatement(char* funcname,char* file_name,int argtype,char* method){
	ParserInfo ifp;
	Token iftoken=GetNextToken();
	if(!strcmp(iftoken.lx,"if"));
	else{
		ifp.er=15;
		ifp.tk=iftoken;
		return ifp;
	}
	iftoken=GetNextToken();
	if(!strcmp(iftoken.lx,"("));
	else{
		ifp.er=11;
		ifp.tk=iftoken;
		return ifp;
	}
	ifp=expression(file_name,funcname,argtype,method);
	if(ifp.er!=0){
		return ifp;
	}
	iftoken=GetNextToken();
	if(!strcmp(iftoken.lx,")"));
	else{
		ifp.er=12;
		ifp.tk=iftoken;
		return ifp;
	}
	if(passnumber==1){
		char ifs[128];
		sprintf(ifs,"%d",timesrun);
		fputs("if-goto IF_TRUE",new_file);
		fputs(ifs,new_file);
		fputs("\n",new_file);
		fputs("goto IF_FALSE",new_file);
		fputs(ifs,new_file);
		fputs("\n",new_file);
		fputs("label IF_TRUE",new_file);
		fputs(ifs,new_file);
		fputs("\n",new_file);
		timesrun+=1;
	}
	iftoken=GetNextToken();
	if(!strcmp(iftoken.lx,"{"));
	else{
		ifp.er=4;
		ifp.tk=iftoken;
		return ifp;
	}
	iftoken=PeekNextToken();
	while(!strcmp(iftoken.lx,"var")||!strcmp(iftoken.lx,"let")||!strcmp(iftoken.lx,"if")||!strcmp(iftoken.lx,"while")||!strcmp(iftoken.lx,"do")||!strcmp(iftoken.lx,"return")){
		ifp=statement(funcname,file_name,argtype,method);
		iftoken=PeekNextToken();
		if(ifp.er!=0)
			return ifp;
	}
	iftoken=GetNextToken();
	if(!strcmp(iftoken.lx,"}"));
	else{
		ifp.er=5;
		ifp.tk=iftoken;
		return ifp;
	}
	if(passnumber==1){
		timesrun--;
	}
	iftoken=PeekNextToken();
	if(!strcmp(iftoken.lx,"else")){
		if(passnumber==1){
			char ifs2[128];
			sprintf(ifs2,"%d",timesrun);
			fputs("goto IF_END",new_file);
			fputs(ifs2,new_file);
			fputs("\n",new_file);
			fputs("label IF_FALSE",new_file);
			fputs(ifs2,new_file);
			fputs("\n",new_file);
		}
		iftoken=GetNextToken();
		iftoken=GetNextToken();
		if(!strcmp(iftoken.lx,"{"));
		else{
			ifp.er=4;
			ifp.tk=iftoken;
			return ifp;
		}
		iftoken=PeekNextToken();
		while(!strcmp(iftoken.lx,"var")||!strcmp(iftoken.lx,"let")||!strcmp(iftoken.lx,"if")||!strcmp(iftoken.lx,"while")||!strcmp(iftoken.lx,"do")||!strcmp(iftoken.lx,"return")){
			ifp=statement(funcname,file_name,argtype,method);
			iftoken=PeekNextToken();
			if(ifp.er!=0)
				return ifp;
		}
		iftoken=GetNextToken();
		if(!strcmp(iftoken.lx,"}"));
		else{
			ifp.er=5;
			ifp.tk=iftoken;
			return ifp;
		}
		if(passnumber==1){
			char ifs3[128];
			sprintf(ifs3,"%d",timesrun);
			fputs("label IF_END",new_file);
			fputs(ifs3,new_file);
			fputs("\n",new_file);
		}
	}
	else{
		if(passnumber==1){
			char ifs2[128];
			sprintf(ifs2,"%d",timesrun);
			fputs("label IF_FALSE",new_file);
			fputs(ifs2,new_file);
			fputs("\n",new_file);
		}
	}
	return ifp;

}
ParserInfo whileStatement(char* funcname,char* file_name,int argtype,char* method){
	ParserInfo whp;
	Token whltoken=GetNextToken();
	if(!strcmp(whltoken.lx,"while"));
	else{
		whp.er=15;
		whp.tk=whltoken;
		return whp;
	}
	whltoken=GetNextToken();
	if(!strcmp(whltoken.lx,"("));
	else{
		whp.er=11;
		whp.tk=whltoken;
		return whp;
	}
	if(passnumber==1){
		fputs("label WHILE_EXP0\n",new_file);
	}
	whp=expression(file_name,funcname,argtype,method);
	if(whp.er!=0){
		return whp;
	}
	whltoken=GetNextToken();
	if(!strcmp(whltoken.lx,")"));
	else{
		whp.er=12;
		whp.tk=whltoken;
		return whp;
	}
	if(passnumber==1){
		fputs("not\n",new_file);
		fputs("if-goto WHILE_END0\n",new_file);
	}
	whltoken=GetNextToken();
	if(!strcmp(whltoken.lx,"{"));
	else{
		whp.er=4;
		whp.tk=whltoken;
		return whp;
	}
	whltoken=PeekNextToken();
	while(!strcmp(whltoken.lx,"var")||!strcmp(whltoken.lx,"let")||!strcmp(whltoken.lx,"if")||!strcmp(whltoken.lx,"while")||!strcmp(whltoken.lx,"do")||!strcmp(whltoken.lx,"return")){
		whp=statement(funcname,file_name,argtype,method);
		whltoken=PeekNextToken();
		if(whp.er!=0)
			return whp;
		}
	whltoken=GetNextToken();
	if(!strcmp(whltoken.lx,"}"));
	else{
		whp.er=5;
		whp.tk=whltoken;
		return whp;
	}
	if(passnumber==1){
		fputs("goto WHILE_EXP0\n",new_file);
		fputs("label WHILE_END0\n",new_file);
	}
	return whp;

}
ParserInfo doStatement(char* file_name,char* funcname,int argtype,char* method){
	ParserInfo dop;
	dop.er=0;
	Token dotoken=GetNextToken();
	if(!strcmp(dotoken.lx,"do"));
	else{
		dop.er=15;
		dop.tk=dotoken;
		return dop;
	}
	dop=subroutinerall(file_name,funcname,argtype,method);
	if(dop.er!=0){
		return dop;
	}
	dotoken=GetNextToken();
	if(!strcmp(dotoken.lx,";"));
	else{
		dop.er=9;
		dop.tk=dotoken;
		return dop;
	}
	return dop;
}

ParserInfo subroutinerall(char* file_name,char* funcname,int argtype,char* method){
	ParserInfo subcp;
	subcp.er=0;
	int run;
	run=0;
	Token subctoken=GetNextToken();
	//printf("%s %d start\n",subcp.tk.lx,subcp.er);
	char command [128];
	char function[128];
	char file[128];
	strcpy(function," ");
	int kind;
	int idnum;
	if(subctoken.tp==ID)
	{
		if(passnumber==1){	
			int find;
			find=FindSymbol(subctoken.lx);
			if(find==-1){
				subcp.er=16;
				subcp.tk=subctoken;
				return subcp;
			}
			kind=is_local(subctoken.lx,funcname,file_name);
			if(kind==-1){
				int num;
				kind=kind_out(subctoken.lx,file_name,file_name);
				num=number_out(subctoken.lx,file_name,file_name);
				if(kind==1){
					char numstr[128];
					sprintf(numstr,"%d",num);
					fputs("push this ",new_file);
					fputs(numstr,new_file);
					fputs("\n",new_file);

				}
				strcpy(command,subctoken.lx);
				strcpy(file,subctoken.lx);
			}
			else{
				int number;
				number=number_out(subctoken.lx,funcname,file_name);
				char numstr[128];
				sprintf(numstr,"%d",number);
				fputs("push local ",new_file);
				fputs(numstr,new_file);
				fputs("\n",new_file);
				fputs("call ",new_file);
				idnum=add_line(subctoken.lx);
			}
			strcpy(file,subctoken.lx);
		}	
	}
	
	else{
		subcp.er=3;
		subcp.tk=subctoken;
		return subcp;
	}
	
	subctoken=PeekNextToken();
	if(!strcmp(subctoken.lx,".")){
		if(passnumber==1){
			strcat(command,".");
		}
		subctoken=GetNextToken();
		subctoken=GetNextToken();
		if(subctoken.tp==ID){
			if(passnumber==1){
				int find;
				find=FindSymbol(subctoken.lx);
				if (find==-1){
					subcp.er=16;
					subcp.tk=subctoken;
					return subcp;
				}
				find=FindSymbol(subctoken.lx);
				if (find==-1){
					subcp.er=16;
					subcp.tk=subctoken;
					return subcp;
				}
				strcat(command,subctoken.lx);
				strcpy(function,subctoken.lx);
				run=1;
			}
		}
		else{
			subcp.er=3;
			subcp.tk=subctoken;
			return subcp;
		}
	}
	subctoken=GetNextToken();
	if(!strcmp(subctoken.lx,"("));
	else{
		subcp.er=11;
		subcp.tk=subctoken;
		return subcp;
	}
	if(passnumber==1){
		int field;
		field=kind_out(file,file_name,file_name);
		if(((!strcmp(method,"method")||!strcmp(method,"constructor"))&&strcmp(file,"String")&&strcmp(file,"Screen")&&strcmp(file,"Output")&&!strcmp(funcname,"new")&&field!=1)){
			fputs("push pointer 0\n",new_file);
		}
		if(!strcmp(method,"method")&&run==0){
			fputs("push pointer 0\n",new_file);
		}
	}
	subcp=expressionList(file_name,funcname,argtype,method);
	if(subcp.er!=0){
		return subcp;
	}
	subctoken=GetNextToken();
	if(!strcmp(subctoken.lx,")"));
	else{
		subcp.er=12;
		subcp.tk=subctoken;
		return subcp;
	}
	if(passnumber==1){
		int args;
		char argstr[128];
		int kind;
		int field;
		field=kind_out(file,file_name,file_name);
		kind=is_local(file,funcname,file_name);
		if(kind==-1){
			kind=FindSymbol(function);
			if(kind==-1){
				printf("%s\n",file);
				fputs("call ",new_file);
				fputs(file_name,new_file);
				fputs(".",new_file);
				fputs(file,new_file);
				args=num_var(file,file_name,2)+1;
			}
			else if(field==1){
				int id_num;
				fputs("call ",new_file);
				id_num=add_line(file);
				fputs(".",new_file);
				fputs(function,new_file);
				args=ext_args(function,id_num)+1;
			}
			else{
				fputs("call ",new_file);
				fputs(file,new_file);
				fputs(".",new_file);
				fputs(function,new_file);
				args=num_var(function,file,2);
			}
		}
		else{
			if(!strcmp("Memory",file)||!strcmp("Output",file)||!strcmp("Keyboard",file)||!strcmp("Math",file)||!strcmp("Screen",file)||!strcmp("Sys",file)||!strcmp("String",file)){
				fputs("call ",new_file);
				fputs(file,new_file);
			}
			fputs(".",new_file);
			fputs(function,new_file);
			args=ext_args(function,idnum)+1;
			
		}
		sprintf(argstr,"%d",args);
		fputs(" ",new_file);
		fputs(argstr,new_file);
		fputs("\n",new_file);
		fputs("pop temp 0\n",new_file);
	}
	return subcp;

}
ParserInfo returnStatement(char* file_name,char* funcname,int argtype,char* method){
	ParserInfo retp;
	retp.er=0;
	char id[128];
	strcpy(id,"a");
	Token rettoken=GetNextToken();
	if(!strcmp(rettoken.lx,"return"));
	else{
		retp.er=15;
		retp.tk=rettoken;
		return retp;
	}
	rettoken=PeekNextToken();
	if(!strcmp(rettoken.lx,"-")||!strcmp(rettoken.lx,"~")||!strcmp(rettoken.lx,"true")||!strcmp(rettoken.lx,"false")||!strcmp(rettoken.lx,"this")||!strcmp(rettoken.lx,"null")||!strcmp(rettoken.lx,"(")||rettoken.tp==STRING||rettoken.tp==ID||rettoken.tp==INT){
		retp=expression(file_name,funcname,argtype,method);
		if(retp.er!=0){
			return retp;
		}
		strcpy(id,"b");
	}
	rettoken=GetNextToken();
	if(!strcmp(rettoken.lx,";"));
	else{
		retp.er=9;
		retp.tk=rettoken;
		return retp;
	}
	if(passnumber==1){
		if(!strcmp(id,"b"));
		else{
			fputs("push constant 0\n",new_file);
		}
		fputs("return\n",new_file);
		strcpy(id,"b");
	}
	
	return retp;
}
ParserInfo paramList(char* funcname,char* file_name){
	ParserInfo paramp;
	DataTypes paramtype;
	Token paramtoken=PeekNextToken();
	if(!strcmp(paramtoken.lx,"int")||!strcmp(paramtoken.lx,"boolean")||!strcmp(paramtoken.lx,"char")||paramtoken.tp==ID){
		paramtype=type();
		if(paramtype==4){
			paramp.er=8;
			paramp.tk=paramtoken;
			return paramp;
		}
	}
	else{
		return paramp;
	}
	paramtoken=GetNextToken();
	if(paramtoken.tp==ID){
		if(passnumber==0){
			insymbol(paramtoken.lx,paramtype,2,funcname,file_name);
		}
	}
	else{
		paramp.er=3;
		paramp.tk=paramtoken;
		return paramp;
	}
	paramtoken=PeekNextToken();
	while(!strcmp(paramtoken.lx,",")){
		paramtoken=GetNextToken();
		paramtype=type();
		if(paramtype==4){
			paramp.er=8;
			paramp.tk=paramtoken;
			return paramp;
		}
		paramtoken=GetNextToken();
		if(paramtoken.tp==ID){
			if(passnumber==0){
				insymbol(paramtoken.lx,paramtype,2,funcname,file_name);
			}
		}
		else{
			paramp.er=3;
			paramp.tk=paramtoken;
			return paramp;
		}
		paramtoken=PeekNextToken();

	}
	return paramp;
}

ParserInfo Parse (char* filename)
{
	ParserInfo pi;
	pi.er=0;
	// implement the function
	InitParser(filename);
	pi.er=glpi.er;
	if(!(glpi.er==0)){
		pi.tk=glpi.tk;
	//	printf("error: %d\n",pi.er);
	//printf("lexeme %s\n",pi.tk.lx);
	//printf("line: %d\n",pi.tk.ln);
	}
	//printf("%s %d\n",pi.tk.lx,pi.er);
	return pi;
}


int StopParser ()
{
	return 1;
}

#ifndef TEST_PARSER
/*int main (int argc, char* argv[])
{
	Parse("Ball.jack");
	return 1;
}*/
#endif
