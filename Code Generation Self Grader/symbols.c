#include "symbols.h"
#include <string.h>
#include <stdio.h>
Symbol symtab[1000];
Extra exttab[1000];
int numsym,numext;
extern FILE *new_file;

void initsymtab(){
    numsym=0;
}
void initext(){
  numext=0;
}

void insymbol(char* name,DataTypes type,Kind kind,char* function,char* file){
    symtab[numsym].type=type;
    strcpy(symtab[numsym].varname,name);
    strcpy(symtab[numsym].function,function);
    strcpy(symtab[numsym].file,file);
    symtab[numsym].kind=kind;
    int count;
    count=0;
    int i;
    for(i=0;i<numsym;i++){
      if(symtab[i].kind==kind){
        if(!strcmp(symtab[i].function,function)){
          if(!strcmp(symtab[i].file,file)){
            count+=1;
          }
        }
      }
    }
    symtab[numsym].number=count;
    numsym++;
}
void in_ext(char* name,char* type){
  strcpy(exttab[numext].varname,name);
  strcpy(exttab[numext].type,type);
  numext++;
}
int ext_args(char* function,int number){
  int i;
  int local;
  local=0;
  for(i=0;i<numsym;i++){
    if(!strcmp(symtab[i].function,function)&&!strcmp(exttab[number].type,symtab[i].file)&&symtab[i].kind==2){
      local+=1;
    }
  }
  return local;
}
int add_line(char* name){
  int i;
  for(i=0;i<numext;i++){
    if(!strcmp(name,exttab[i].varname)){
      char type[128];
      strcpy(type,exttab[i].type);
      fputs(type,new_file);
      return i;
    }
  }
  return -1;
}
int FindSymbol(char* name){
  int i;
  if(numsym==0){
    return -1;
  }
  for(i=0;i<numsym;i++){
      if(!strcmp(symtab[i].varname,name)){
          return i;
      }
  }
  return -1;
}
int find_infunc(char* name,char* function){
  int i;
  if(numsym==0){
    return -1;
  }
  for(i=0;i<numsym;i++){
      if(!strcmp(symtab[i].varname,name)){
          if(!strcmp(symtab[i].function,function)){
            return i;
          }
      }
  }
  return -1;
}
int functionsame(char* name,char* function){
  int find;
  find=FindSymbol(name);
  if(find!=-1){
    if(!strcmp(symtab[find].function,function)){
      return 1;
    }
  }
  return -1;
}

int subcheck(char * name){
  int find;
  find=FindSymbol(name);
  if(find==-1){
    return -1;
  }
  if(symtab[find].kind==4||symtab[find].kind==5){
    return 1;
  }
  return -1;
  
}
int num_var(char* function,char* file,int type){
  int i;
  int local;
  local=0;
  for(i=0;i<numsym;i++){
    if(!strcmp(symtab[i].function,function)&&!strcmp(symtab[i].file,file)&&symtab[i].kind==type){
      local+=1;
    }
  }
  return local;
}

int kind_out(char* name,char* function,char* file){
  int i;
  for(i=0;i<numsym;i++){
    if(!strcmp(symtab[i].function,function)){
      if(!strcmp(symtab[i].file,file)){
        if(!strcmp(symtab[i].varname,name)){
          int kind;
          kind=symtab[i].kind;
          return kind;
        }
      }
    }
  }
  return -1;
}
int number_out(char* name,char* function,char* file){
  int i;
  for(i=0;i<numsym;i++){
    if(!strcmp(symtab[i].function,function)){
      if(!strcmp(symtab[i].file,file)){
        if(!strcmp(symtab[i].varname,name)){
          int number;
          number=symtab[i].number;
          return number;
        }
      }
    }
  }
  return -1;
}
int class_kind(char* name,char* file){
    int i;
    for(i=0;i<numsym;i++){
      if(!strcmp(symtab[i].file,file)){
        if(!strcmp(symtab[i].varname,name)){
          int kind;
          kind=symtab[i].kind;
          return kind;
        }
      }
    }
  return -1;
}
int class_num(char* name,char* file){
    int i;
    for(i=0;i<numsym;i++){
      if(!strcmp(symtab[i].file,file)){
        if(!strcmp(symtab[i].varname,name)){
          int number;
          number=symtab[i].number;
          return number;
        }
      }
    }
  return -1;
}
int is_constructor(char* file){
  int i;
  for(i=0;i<numsym;i++){
    if(!strcmp(symtab[i].file,file)&&!strcmp(symtab[i].function,"new")){
      return 1;
    }
  }
  return -1;
}
int is_file_name(char* name){
  int i;
  for(i=0;i<numsym;i++){
    if(!strcmp(symtab[i].file,name)&&!strcmp(symtab[i].varname,name)){
      return 0;
    }
    if(!strcmp(symtab[i].function,name)&&!strcmp(symtab[i].varname,name)){
      return 1;
    }
  }
  return -1;
}
int is_local(char* name,char* function,char* file){
  int i;
  for(i=0;i<numsym;i++){
    if(!strcmp(symtab[i].varname,name)&&!strcmp(symtab[i].function,function)&&!strcmp(symtab[i].file,file)&&symtab[i].kind==3){
      return i;
    }
  }
  return -1;
}
int func_number_out(int location){
  int number;
  number=symtab[location].number;
  return number;
}