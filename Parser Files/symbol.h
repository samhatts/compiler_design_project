typedef enum {integer,boolean,character,class,err,} DataTypes;
typedef enum {static,field,constructor,method,function}clsDataTypes;
typedef struct{
    char varname[128];
    DataTypes type;
    clsDataTypes functype;
    char kind[128];
    int number;
    int startnumber;
    int endnumber;
} Symbol;
void insymbol(char*,DataTypes);
int FindSymbol(char* name);
