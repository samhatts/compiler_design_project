#include "symbol.h"
#include <string.h>
Symbol symtab[128];
Symbol mettab[128];
int numsym;
int metsym;

void initsymtab(){
    numsym=0;
    nummet=0;
}

void insymbol(char* name,DataTypes type,char* kind){
    symtab[numsym].type=type;
    strcpy(symtab[numsym].varname,name);
    strcpy(symtab[numsym].kind,kind);
    int i,count;
    count=-1;
    for(i=0;i<numsym;i++){
      if(!strcmp(symtab[i].varname,name)){
        count+=1;
      }
    }
    symtab[numsym].number=count;
    symtab[numsym].startnumber=numsym;
    symtab[numsym].endnumber=numsym;
    numsym++;
}
void infunccls(char*name,DataTypes type,char* kind, clsDataTypes functype){
   insymbol();
   strcpy(symtab[numsym].functype,functype);
}
void addfuncvar(char*name,DataTypes type,char* kind,clsDataTypes functype){
    mettab[nummet].type=type;
    strcpy(mettab[nummet].varname,name);
    strcpy(mettab[nummet].kind,kind);
    strcpy(mettab[nummet].functype,functype);
    int temp=0;
    int temp2=0;
    temp=nummet-1;
    if(!strcmp(mettab[temp].functype,mettab[nummet].functype)){
      mettab[nummet].startnumber=mettab[temp].startnumber;
      temp2=mettab[temp].endnumber+1
      mettab[nummet].endnumber=temp2;
    }
    else{
      mettab[nummet].startnumber=nummet;
      mettab[nummet].endnumber=nummet;
    }
    int start=mettab[nummet].startnumber;
    int end=mettab[nummet].endnumber;
    int i,count;
    count=-1;
    for(i=start;i<end;i++){
      if(!strcmp(mettab[i].varname,name)){
        count+=1;
      }
    }
    mettab[numsym].number=count;

}
int FindmetSymbol(char*name,char* funcname){
    int i,start,end;
    for(i=0;i<numsym;i++){
        if(!strcmp(symtab[i].varname,funcname)){
            for(j=0;i<nummet;j++){
              if(!strcmp(mettab[j].varname,name)){
                return 1;
              }
              else{
                j=mettab[j].endnumber;
              }
            }
        }
    }
    return -1;
}
int FindSymbol(char* name){
  int i;
  for(i=0;i<numsym;i++){
      if(!strcmp(symtab[i].varname,name)){
          return i;
      }
  }
  return -1;
}
