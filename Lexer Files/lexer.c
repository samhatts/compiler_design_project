/************************************************************************
University of Leeds
School of Computing
COMP2932- Compiler Design and Construction
Lexer Module

I confirm that the following code has been developed and written by me and it is entirely the result of my own work.
I also confirm that I have not copied any parts of this program from another person or any other source or facilitated someone to copy this program from me.
I confirm that I will not publish the program online or share it with anyone without permission of the module leader.

Student Name:Samuel David Hattersley
Student ID:201530741
Email:sc21sdh@leeds.ac.uk
Date Work Commenced:17/02/23
*************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "lexer.h"

// YOU CAN ADD YOUR OWN FUNCTIONS, DECLARATIONS AND VARIABLES HERE
FILE *src_file;
int linecounter;
char globalfl[32];
char *recall;
const char *keywords[] = {"class", "field", "int", "constructor", "let", "return", "this", "method", "void", "while", "null", "do", "boolean", "true", "false", "var", "if", "else", "function", "char", "static"};
int eofchecker(int newtoken)
{
  if (newtoken == EOF)
  {
    return 1;
  }
  return 0;
}
void linecount_func(int newtoken)
{
  if (newtoken == '\n')
  {
    linecounter += 1;
    if (newtoken == '\t')
    {
      linecounter--;
    }
  }
}

// IMPLEMENT THE FOLLOWING functions
//***********************************

// Initialise the lexer to read from source file
// file_name is the name of the source file
// This requires opening the file and making any necessary initialisations of the lexer
// If an error occurs, the function should return 0
// if everything goes well the function should return 1
int InitLexer(char *file_name)
{
  linecounter = 1;
  src_file = fopen(file_name, "r");
  strcpy(globalfl, file_name);
  if (src_file == 0)
  {
    printf("File cannot open or does not exist");
    return 0;
  }
  return 1;
}

// Get the next token from the source file
Token GetNextToken()
{
  Token t; // intialising variables
  t.tp = ERR;
  strcpy(t.fl, globalfl);
  int ntoken;
  char temp[128];
  int i = 0;
  int start = 1;
  ntoken = getc(src_file); // gets next character
  while (isspace(ntoken) || ntoken == '/')
  { // removes leading comments and spaces
    if (start != 1)
    {
      ntoken = getc(src_file);
    }
    else
    {
      start = 0;
    }
    if (ntoken == '\n')
    {
      linecount_func(ntoken);
    }
    if (ntoken == '/')
    { // removes comments
      ntoken = getc(src_file);
      if (ntoken == '/')
      { // single line
        while (ntoken != '\n')
        {
          ntoken = getc(src_file);
        }
        linecount_func(ntoken);
      }
      else if (ntoken == '*')
      { // multi-line
        ntoken = getc(src_file);
        ntoken = getc(src_file);
        linecount_func(ntoken);
        while (1)
        {
          while (ntoken != '*')
          {
            ntoken = getc(src_file);
            linecount_func(ntoken);
            if (eofchecker(ntoken) == 1)
            {
              t.ln = linecounter;
              t.ec = 0;
              strcpy(t.lx, "Error: unexpected eof in comment");
              return t;
            }
          }
          ntoken = getc(src_file);
          linecount_func(ntoken);
          if (ntoken == '/')
          {
            break;
          }
        }
      }
      else
      { // not actually a comment is a division symbol
        ungetc(ntoken, src_file);

        if (ntoken == '\n')
        {
          linecounter--;
        }
        t.tp = SYMBOL;
        temp[0] = '/';
        temp[1] = '\0';
        t.ln=linecounter;
        strcpy(t.lx, temp);
        return t;
      }
    }
  }
  if (ntoken == EOF)
  { // checks if EOF
    strcpy(t.lx, "EOF");
    t.tp = EOFile;
    t.ln = linecounter;
    return t;
  }
  if (isalpha(ntoken) || ntoken == '_')
  { // checks for ID and RESWORD
    while (isalpha(ntoken) || ntoken == '_' || isdigit(ntoken))
    { // gets string making up ID/RESWORD
      temp[i] = ntoken;
      i++;
      ntoken = getc(src_file);
      linecount_func(ntoken);
    }
    if(ntoken=='\n'){
      linecounter--;
    }
    ungetc(ntoken, src_file);
    temp[i] = '\0';
    t.ln = linecounter;
    strcpy(t.lx, temp);
    for (i = 0; i < 21; i++)
    { // checks to see if in resword array
      if (strcmp(temp, keywords[i]) == 0)
      {
        t.tp = RESWORD;
        t.ln = linecounter;
        return t;
      }
    }
    t.tp = ID;
    return t;
  }
  else if (isdigit(ntoken)) // checks if number
  {
    while (isdigit(ntoken))
    {
      temp[i] = ntoken;
      i++;
      ntoken = getc(src_file);
      linecount_func(ntoken);
    }
    if(ntoken=='\n'){
      linecounter--;
    }
    ungetc(ntoken, src_file);
    temp[i] = '\0';
    t.ln = linecounter;
    strcpy(t.lx, temp);
    t.tp = INT;
  }
  else if (ntoken == '"')
  { // checks if string
    ntoken = getc(src_file);
    while (ntoken != '"')
    {
      temp[i] = ntoken;
      i++;
      ntoken = getc(src_file);
      t.ln = linecounter;
      if (ntoken == '\n')
      {
        t.ec = 1;
        strcpy(t.lx, "Error: new line in string constant");
        return t;
      }
      if (eofchecker(ntoken) == 1)
      {
        t.ec = 2;
        strcpy(t.lx, "Error: unexpected eof in string constant");
        return t;
      }
    }
    temp[i] = '\0';
    t.ln = linecounter;
    strcpy(t.lx, temp);
    t.tp = STRING;
    strcpy(t.lx, temp);
  }
  else // checks symbols
  {
    t.ln = linecounter;
    if (ntoken == '?')
    { // makes sure the illegal ? isn't there
      t.ec = 3;
      strcpy(t.lx, "Error: illegal symbol in source file");
    }
    else
    {
      t.tp = SYMBOL;
      temp[0] = ntoken;
      temp[1] = '\0';
      strcpy(t.lx, temp);
    }
    return t;
  }
  return t;
}

// peek (look) at the next token in the source file without removing it from the stream
Token PeekNextToken()
{
  Token t;
  int i;
  t = GetNextToken();
  if(t.tp==EOFile){
    return t;
  }
  i = 0;
  while (t.lx[i] != '\0')
  {
    i++;
  }
  if (t.tp == STRING)
  {
    ungetc('"', src_file);
  }
  i--;
  while (i != -1)
  {
    ungetc(t.lx[i], src_file);
    i--;
  }
  if (t.tp == STRING)
  {
    ungetc('"', src_file);
  }
  linecounter=linecounter-i-1;
  return t;
}

// clean out at end, e.g. close files, free memory, ... etc
int StopLexer()
{
  fclose(src_file);
  return 0;
}

// do not remove the next line
#ifndef TEST
int main()
{

    // implement your main function here
    // NOTE: the autograder will not use your main function

  return 0;
}
// do not remove the next line
#endif
