#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "lexer.h"
#include "parser.h"
#include "symbol.h"


// you can derlare prototypes of parser functions below
ParserInfo classDeclar();
ParserInfo memberDeclar();
ParserInfo classVarDeclar();
ParserInfo type();
ParserInfo subroutineDeclar();
ParserInfo paramList();
ParserInfo subroutineBody();
ParserInfo statement();
ParserInfo varDeclarStatement();
ParserInfo letStatement();
ParserInfo ifStatement();
ParserInfo whileStatement();
ParserInfo doStatement();
ParserInfo subroutinerall();
ParserInfo expressionList();
ParserInfo returnStatement();
ParserInfo expression();
ParserInfo relationalExpression();
ParserInfo arithmeticExpression();
ParserInfo term();
ParserInfo factor();
ParserInfo operand();
ParserInfo glpi;


int InitParser (char* file_name)
{
	InitLexer(file_name);
	Token token=PeekNextToken();
	printf("start\n");
	while(token.tp!=EOFile){
		glpi=classDeclar();
		token=PeekNextToken();
		printf("%d ",glpi.er);
	}
	return 1;
}
ParserInfo classDeclar(){
	ParserInfo p;
	p.er=0;
	Token clstoken=GetNextToken();
	p.tk=clstoken;
	if(!strcmp(clstoken.lx,"class"));
	else{
		p.er=2;
		return p;
	}
	clstoken=GetNextToken();
	if(clstoken.tp==ID){
		;
	else{
		p.tk=clstoken;
		p.er=3;
		return p;
	}
	clstoken=GetNextToken();
	if(!strcmp(clstoken.lx,"{"));
	else{
		p.tk=clstoken;
		p.er=4;
		return p;
	}
	clstoken=PeekNextToken();
	if(!strcmp(clstoken.lx,"static")||!strcmp(clstoken.lx,"field")||!strcmp(clstoken.lx,"static")||!strcmp(clstoken.lx,"constructor")||!strcmp(clstoken.lx,"method")||!strcmp(clstoken.lx,"function")){
		p=memberDeclar();
	}
	clstoken=GetNextToken();
	if(!strcmp(clstoken.lx,"}"));
	else{
		p.tk=clstoken;
		p.er=5;
	}
	return p;
}
ParserInfo memberDeclar(){
	ParserInfo pm;
	pm.er=0;
	Token mt=PeekNextToken();
	pm.tk=mt;
	if(!strcmp(mt.lx,"static")||!strcmp(mt.lx,"field")){
		pm=classVarDeclar();
		return pm;
	}
	else if(!strcmp(mt.lx,"constructor")||!strcmp(mt.lx,"method")||!strcmp(mt.lx,"function")){
		pm=subroutineDeclar();
		return pm;
	}
	else{
		pm.er=6;
		pm.tk=mt;
		return pm;
	}
}
ParserInfo classVarDeclar(){
	ParserInfo pc;
	pc.er=0;
	Token clt=GetNextToken();
	ParserInfo clttype=type();
	if (clttype.ec==8){
		pc.er=8;
		pc.tk=clt;
		return pc;
	}
	clt=GetNextToken();
	if(clt.tp==ID){
		int find=FindSymbol(clt.lx);
		if(find!=-1){
			pc.er=15;
			pc.tk=clt;
			return pc;
		}
		insymbol(clt.lx,clttype);
	}
	else{
		pc.er=3;
		pc.tk=clt;
		return pc;
	}
	clt=PeekNextToken();
	while(!strcmp(clt.lx,",")){
		clt=GetNextToken();
		clt=GetNextToken();
		if(clt.tp==ID){
			int find=FindSymbol(clt.lx);
			if(find!=-1){
				pc.er=15;
				pc.tk=clt;
				return pc;
			}
			insymbol(clt.lx,clttype);
		}
		else{
			pc.er=3;
			pc.tk=clt;
			return pc;
		}
		clt=PeekNextToken();
	}
	clt=GetNextToken();
	if(!strcmp(clt.lx,";"));
	else{
		pc.er=9;
	}
	pc.tk=clt;
	return pc;
}
ParserInfo type(){
	ParserInfo pty;
	pty.er=0;
	Token tptoken=GetNextToken();
	pty.tk=tptoken;
	if(!strcmp(tptoken.lx,"int")){
		return pty;
	}
	else if(!strcmp(tptoken.lx,"char")){
		return pty;
	}
	else if(!strcmp(tptoken.lx,"boolean")){
		return pty;
	}
	else if(tptoken.tp==ID){
		return pty;
	}
	else{
		pty.ec=8;
		return pty;
	}
}
ParserInfo subroutineDeclar(){
	ParserInfo ps;
	ps.er=0;
	Token subt=GetNextToken();
	subt=PeekNextToken();
	ps.tk=subt;
	if(!strcmp(subt.lx,"void")){
		Token subt=GetNextToken();
	}
	else{
		ParserInfo subttype=type();
	}
	subt=GetNextToken();
	if(subt.tp==ID);
	else{
		ps.er=3;
		ps.tk=subt;
		return ps;
	}
	subt=GetNextToken();
	if(!strcmp(subt.lx,"(")){
		ps=paramList();
	}
	else{
		ps.er=11;
		ps.tk=subt;
		return ps;
	}
	ps=paramList();
	subt=GetNextToken();
	if(!strcmp(subt.lx,")")){
		ps=paramList();
	}
	else{
		ps.er=12;
		ps.tk=subt;
		return ps;
	}
	ps=subroutineBody();
	return ps;
}
ParserInfo subroutineBody(){
	ParserInfo psb;
	psb.er=0;
	Token subbt=GetNextToken();
	psb.tk=subbt;
	if(!strcmp(subbt.lx,"{"));
	else{
		psb.er=4;
		psb.tk=subbt;
		return psb;
	}
	subbt=PeekNextToken();
	while(!strcmp(subbt.lx,"var")||!strcmp(subbt.lx,"let")||!strcmp(subbt.lx,"if")||!strcmp(subbt.lx,"while")||!strcmp(subbt.lx,"do")||!strcmp(subbt.lx,"return")){
		psb=statement();
		if(psb.er!=0){
			return psb;
		}
		subbt=PeekNextToken();
	}
	subbt=GetNextToken();
	if(!strcmp(subbt.lx,"}"));
	else{
		psb.er=5;
		psb.tk=subbt;
		return psb;
	}
	return psb;
}

ParserInfo statement (){
	ParserInfo pst;
	Token stoken=PeekNextToken();
	if(!strcmp(stoken.lx,"var")){
		pst=varDeclarStatement();
	}
	else if(!strcmp(stoken.lx,"let"))
		pst=letStatement();
	else if(!strcmp(stoken.lx,"if"))
		pst=ifStatement();
	else if(!strcmp(stoken.lx,"while"))
		pst=whileStatement();
	else if(!strcmp(stoken.lx,"do"))
		pst=doStatement();
	else if(!strcmp(stoken.lx,"return"))
		pst=returnStatement();
	else{
		pst.er=15;
		pst.tk=stoken;
	}
	return pst;
	}

ParserInfo varDeclarStatement(){
	ParserInfo vp;
	int vardectype;
	vp.er=0;
	Token vardertoken=GetNextToken();
	if(!strcmp(vardertoken.lx,"var"));
	else{
		vp.er=15;
		vp.tk=vardertoken;
		return vp;
	}
	vardectype=type();
	if(vardectype==4){
		vp.er=8;
		vp.tk=vardertoken;
	}
	vardertoken=GetNextToken();
	if(vardertoken.tp==ID);
	else{
		vp.er=3;
		vp.tk=vardertoken;
		return vp;
	}
	vardertoken=PeekNextToken();
	while(!strcmp(vardertoken.lx,",")){
		vardertoken=GetNextToken();
		vardertoken=GetNextToken();
		if(vardertoken.tp==ID);
		else{
			vp.er=3;
			vp.tk=vardertoken;
			return vp;
		}
		vardertoken=PeekNextToken();
}
vardertoken=GetNextToken();
if(!strcmp(vardertoken.lx,";"));
else{
	vp.er=9;
	vp.tk=vardertoken;
	return vp;
}
return vp;
}
ParserInfo letStatement(){
	ParserInfo ltp;
	ltp.er=0;
	Token lettoken=GetNextToken();
	if(!strcmp(lettoken.lx,"let"))
		;
	else{
		ltp.er=15;
		ltp.tk=lettoken;
		return ltp;
	}
	lettoken=GetNextToken();
	if(lettoken.tp==ID);
	else{
		ltp.er=3;
		ltp.tk=lettoken;
		return ltp;
	}
	lettoken=PeekNextToken();
	if(!strcmp(lettoken.lx,"[")){
		lettoken=GetNextToken();
		lettoken=GetNextToken();
		if(lettoken.tp==ID);
		else{
			ltp.er=3;
			ltp.tk=lettoken;
			return ltp;
		}
		lettoken=GetNextToken();
		if(!strcmp(lettoken.lx,"]"));
		else{
			ltp.er=13;
			ltp.tk=lettoken;
			return ltp;
		}
	}
	lettoken=GetNextToken();
	if(!strcmp(lettoken.lx,"="));
	else{
		ltp.er=14;
		ltp.tk=lettoken;
		return ltp;
	}
	ltp=expression();
	lettoken=GetNextToken();
	if(!strcmp(lettoken.lx,";"));
	else{
		ltp.er=9;
		ltp.tk=lettoken;
		return ltp;
	}
	return ltp;
}
ParserInfo expression(){
	ParserInfo expp;
	expp.er=0;
	Token exptoken=PeekNextToken();
	expp=relationalExpression();
	if(expp.er!=0){
		return expp;
	}
	while (!strcmp(exptoken.lx,"&")||!strcmp(exptoken.lx,"|")){
		exptoken=GetNextToken();
		expp=relationalExpression();
		exptoken=PeekNextToken();
		if(expp.er!=0){
			return expp;
		}
	}
	return expp;
}
ParserInfo relationalExpression(){
	ParserInfo relp;
	relp.er=0;
	Token reltoken=PeekNextToken();
	relp=arithmeticExpression();
	if(relp.er!=0){
		return relp;
	}
	reltoken=PeekNextToken();
	while (!strcmp(reltoken.lx,"=")||!strcmp(reltoken.lx,"<")||!strcmp(reltoken.lx,">")){
		reltoken=GetNextToken();
		relp=arithmeticExpression();
		reltoken=PeekNextToken();
		if(relp.er!=0){
			return relp;
		}
	}
	return relp;
}
ParserInfo arithmeticExpression(){
	ParserInfo artp;
	artp.er=0;
	Token arithtoken=PeekNextToken();
	artp=term();
	if(artp.er!=0){
		return artp;
	}
	arithtoken=PeekNextToken();
	while (!strcmp(arithtoken.lx,"+")||!strcmp(arithtoken.lx,"-")){
		arithtoken=GetNextToken();
		artp=term();
		arithtoken=PeekNextToken();
		if(artp.er!=0){
			return artp;
		}
	}
return artp;
}
ParserInfo term(){
	ParserInfo terp;
	terp.er=0;
	Token termtoken=PeekNextToken();
	terp=factor();
	if(terp.er!=0){
		return terp;
	}
	termtoken=PeekNextToken();
	while (!strcmp(termtoken.lx,"*")||!strcmp(termtoken.lx,"/")){
		termtoken=GetNextToken();
		terp=factor();
		termtoken=PeekNextToken();
		if(terp.er!=0){
			return terp;
		}
	}
	return terp;
}
ParserInfo factor(){
	ParserInfo facp;
	facp.er=0;
	Token factoken=PeekNextToken();
	if(!strcmp(factoken.lx,"-")||!strcmp(factoken.lx,"~"))
		factoken=GetNextToken();
	facp=operand();
	return facp;
}
ParserInfo operand(){
	ParserInfo operp;
	operp.er=0;
	Token optoken=GetNextToken();
	if(optoken.tp==INT);
	else if(optoken.tp==ID){
		optoken=PeekNextToken();
		if(!strcmp(optoken.lx,".")){
			optoken=GetNextToken();
			optoken=GetNextToken();
			if(optoken.tp==ID);
			else{
				operp.er=3;
				operp.tk=optoken;
				return operp;
			}
			optoken=PeekNextToken();
		}
		if(!strcmp(optoken.lx,"[")||!strcmp(optoken.lx,"(")){
			optoken=GetNextToken();
			if(!strcmp(optoken.lx,"[")){
				expression();
				optoken=GetNextToken();
				if(!strcmp(optoken.lx,"]"));
				else{
					operp.er=13;
					operp.tk=optoken;
					return operp;
				}
			}
			else if(!strcmp(optoken.lx,"(")){
				expressionList();
				optoken=GetNextToken();
				if(!strcmp(optoken.lx,")"));
				else{
					operp.er=12;
					operp.tk=optoken;
					return operp;
				}
			}
		}
	}
	else if(!strcmp(optoken.lx,"(")){
		operp=expression();
		optoken=GetNextToken();
		if(!strcmp(optoken.lx,")"));
		else{
			operp.er=12;
			operp.tk=optoken;
			return operp;
		}
	}
	else if(optoken.tp==STRING);
	else if(!strcmp(optoken.lx,"true")||!strcmp(optoken.lx,"false")||!strcmp(optoken.lx,"null")||!strcmp(optoken.lx,"this"));
	else {
		operp.er=15;
		operp.tk=optoken;
		return operp;

	}
	return operp;
}
ParserInfo expressionList(){
	ParserInfo explp;
	Token expltoken=PeekNextToken();
	if(!strcmp(expltoken.lx,"-")||!strcmp(expltoken.lx,"~")||!strcmp(expltoken.lx,"true")||!strcmp(expltoken.lx,"false")||!strcmp(expltoken.lx,"null")||!strcmp(expltoken.lx,"this")||!strcmp(expltoken.lx,"(")||expltoken.tp==INT||expltoken.tp==ID||expltoken.tp==STRING){
		explp=expression();
		if(explp.er!=0){
			return explp;
		}
		expltoken=PeekNextToken();
		while(!strcmp(expltoken.lx,",")){
			expltoken=GetNextToken();
			explp=expression();
			if(explp.er!=0){
				return explp;
		}
		}
	}
	return explp;
}
ParserInfo ifStatement(){
	ParserInfo ifp;
	Token iftoken=GetNextToken();
	if(!strcmp(iftoken.lx,"if"));
	else{
		ifp.er=15;
		ifp.tk=iftoken;
		return ifp;
	}
	iftoken=GetNextToken();
	if(!strcmp(iftoken.lx,"("));
	else{
		ifp.er=11;
		ifp.tk=iftoken;
	}
	ifp=expression();
	if(ifp.er!=0){
		return ifp;
	}
	iftoken=GetNextToken();
	if(!strcmp(iftoken.lx,")"));
	else{
		ifp.er=12;
		ifp.tk=iftoken;
	}
	iftoken=GetNextToken();
	if(!strcmp(iftoken.lx,"{"));
	else{
		ifp.er=4;
		ifp.tk=iftoken;
		return ifp;
	}
	iftoken=PeekNextToken();
	while(!strcmp(iftoken.lx,"var")||!strcmp(iftoken.lx,"let")||!strcmp(iftoken.lx,"if")||!strcmp(iftoken.lx,"while")||!strcmp(iftoken.lx,"do")||!strcmp(iftoken.lx,"return")){
		ifp=statement();
		iftoken=PeekNextToken();
		if(ifp.er!=0)
			return ifp;
	}
	iftoken=GetNextToken();
	if(!strcmp(iftoken.lx,"}"));
	else{
		ifp.er=5;
		ifp.tk=iftoken;
		return ifp;
	}
	iftoken=PeekNextToken();
	if(!strcmp(iftoken.lx,"else")){
		iftoken=GetNextToken();
		iftoken=GetNextToken();
		if(!strcmp(iftoken.lx,"{"));
		else{
			ifp.er=4;
			ifp.tk=iftoken;
			return ifp;
		}
		iftoken=PeekNextToken();
		while(!strcmp(iftoken.lx,"var")||!strcmp(iftoken.lx,"let")||!strcmp(iftoken.lx,"if")||!strcmp(iftoken.lx,"while")||!strcmp(iftoken.lx,"do")||!strcmp(iftoken.lx,"return")){
			ifp=statement();
			iftoken=PeekNextToken();
			if(ifp.er!=0)
				return ifp;
		}
		iftoken=GetNextToken();
		if(!strcmp(iftoken.lx,"}"));
		else{
			ifp.er=5;
			ifp.tk=iftoken;
			return ifp;
		}

	}
	return ifp;

}
ParserInfo whileStatement(){
	ParserInfo whp;
	Token whltoken=GetNextToken();
	if(!strcmp(whltoken.lx,"while"));
	else{
		whp.er=15;
		whp.tk=whltoken;
		return whp;
	}
	whltoken=GetNextToken();
	if(!strcmp(whltoken.lx,"("));
	else{
		whp.er=11;
		whp.tk=whltoken;
		return whp;
	}
	whp=expression();
	if(whp.er!=0){
		return whp;
	}
	whltoken=GetNextToken();
	if(!strcmp(whltoken.lx,")"));
	else{
		whp.er=12;
		whp.tk=whltoken;
		return whp;
	}
	whltoken=GetNextToken();
	if(!strcmp(whltoken.lx,"{"));
	else{
		whp.er=4;
		whp.tk=whltoken;
		return whp;
	}
	whltoken=PeekNextToken();
	while(!strcmp(whltoken.lx,"var")||!strcmp(whltoken.lx,"let")||!strcmp(whltoken.lx,"if")||!strcmp(whltoken.lx,"while")||!strcmp(whltoken.lx,"do")||!strcmp(whltoken.lx,"return")){
		whp=statement();
		whltoken=PeekNextToken();
		if(whp.er!=0)
			return whp;
		}
	whltoken=GetNextToken();
	if(!strcmp(whltoken.lx,"}"));
	else{
		whp.er=5;
		whp.tk=whltoken;
		return whp;
	}
	return whp;

}
ParserInfo doStatement(){
	ParserInfo dop;
	Token dotoken=GetNextToken();
	if(!strcmp(dotoken.lx,"do"));
	else{
		dop.er=15;
		dop.tk=dotoken;
		return dop;
	}
	dop=subroutinerall();
	if(dop.er!=0){
		return dop;
	}
	dotoken=GetNextToken();
	if(!strcmp(dotoken.lx,";"));
	else{
		dop.er=9;
		dop.tk=dotoken;
		return dop;
	}
	return dop;
}

ParserInfo subroutinerall(){
	ParserInfo subcp;
	Token subctoken=GetNextToken();
	if(subctoken.tp==ID);
	else{
		subcp.er=3;
		subcp.tk=subctoken;
		return subcp;
	}
	subctoken=PeekNextToken();
	if(!strcmp(subctoken.lx,".")){
		subctoken=GetNextToken();
		subctoken=GetNextToken();
		if(subctoken.tp==ID);
		else{
			subcp.er=3;
			subcp.tk=subctoken;
			return subcp;
		}
	}
	subctoken=GetNextToken();
	if(!strcmp(subctoken.lx,"("));
	else{
		subcp.er=11;
		subcp.tk=subctoken;
		return subcp;
	}
	subcp=expressionList();
	if(subcp.er!=0){
		return subcp;
	}
	subctoken=GetNextToken();
	if(!strcmp(subctoken.lx,")"));
	else{
		subcp.er=12;
		subcp.tk=subctoken;
		return subcp;
	}
	return subcp;

}
ParserInfo returnStatement(){
	ParserInfo retp;
	Token rettoken=GetNextToken();
	if(!strcmp(rettoken.lx,"return"));
	else{
		retp.er=15;
		retp.tk=rettoken;
		return retp;
	}
	rettoken=PeekNextToken();
	if(!strcmp(rettoken.lx,"-")||!strcmp(rettoken.lx,"~")||!strcmp(rettoken.lx,"true")||!strcmp(rettoken.lx,"false")||!strcmp(rettoken.lx,"this")||!strcmp(rettoken.lx,"null")||!strcmp(rettoken.lx,"(")||rettoken.tp==STRING||rettoken.tp==ID||rettoken.tp==INT){
		retp=expression();
	}
	rettoken=GetNextToken();
	if(!strcmp(rettoken.lx,";"));
	else{
		retp.er=9;
		retp.tk=rettoken;
		return retp;
	}
	return retp;
}
ParserInfo paramList(){
	ParserInfo paramp;
	int paramtype;
	Token paramtoken=PeekNextToken();
	if(!strcmp(paramtoken.lx,"int")||!strcmp(paramtoken.lx,"int")||!strcmp(paramtoken.lx,"int")||paramtoken.tp==ID){
		paramtype=type();
		if(paramtype==4){
			paramp.er=8;
			paramp.tk=paramtoken;
			return paramp;
		}
	}
	else{
		return paramp;
	}
	paramtoken=GetNextToken();
	if(paramtoken.tp==ID);
	else{
		paramp.er=3;
		paramp.tk=paramtoken;
		return paramp;
	}
	paramtoken=PeekNextToken();
	while(strcmp(paramtoken.lx,",")){
		paramtoken=GetNextToken();
		paramtype=type();
		if(paramtype==4){
			paramp.er=8;
			paramp.tk=paramtoken;
			return paramp;
		}
		paramtoken=GetNextToken();
		if(paramtoken.tp==ID);
		else{
			paramp.er=3;
			paramp.tk=paramtoken;
			return paramp;
		}
	}
	return paramp;
}

ParserInfo Parse ()
{
	ParserInfo pi;
	// implement the function
	printf("hellio\n");
	InitParser("Main.jack");
	pi.er=glpi.er;
	if(!(glpi.er==0)){
		pi.tk=glpi.tk;
	}
	return pi;
}


int StopParser ()
{
	return 1;
}

#ifndef TEST_PARSER
int main ()
{
	Parse("Main.jack");
	return 1;
}
#endif
