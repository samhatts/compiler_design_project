typedef enum {integer,boolean,character,class,err} DataTypes;
typedef struct{
    char varname[128];
    DataTypes type;
} Symbol;
void insymbol(char*,DataTypes);
int FindSymbol(char* name);
