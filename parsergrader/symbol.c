#include "symbol.h"
#include <string.h>
Symbol symtab[128];
int numsym;

void initsymtab(){
    numsym=0;
}

void insymbol(char* name,DataTypes type){
    symtab[numsym].type=type;
    strcpy(symtab[numsym].varname,name);
    numsym++;
}
int FindSymbol(char*name){
    int i;
    for(i=0;i<numsym;i++){
        if(!strcmp(symtab[i].varname,name)){
            return i;
        }
    }
    return -1;
}
